#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/ledc.h"
#include "esp_wifi.h"
#include "nvs_flash.h"
#include "tcpip_adapter.h"
#include "esp_log.h"

#include "esp_websocket_client.h"
#include "esp_event.h"


// #define myWSSID "INFECAR_ORGANIZACION"
// #define myWPASS "23noviembre"
#define myWSSID "Mighty's phone"
#define myWPASS "supermitel"

#define RESOLUTION 12
#define SERVO_HZ 50
#define SERVO_MIN_PERIOD 1
#define SERVO_MAX_PERIOD 2
#define SERVO_MIN_ANLGLE 0
#define SERVO_MAX_ANGLE 180
#define SERVO_TIMER_INDEX LEDC_TIMER_1
#define NDOF 6

// static const char *WEBSOCKET_ECHO_ENDPOINT = "ws://10.207.252.54:3024"; 
static const char *WEBSOCKET_ECHO_ENDPOINT = "ws://192.168.43.65:3024";



inline int angle2duty(double angle)
{
    int maxn = (1 << RESOLUTION) - 1;
    int T = 1000 / SERVO_HZ;
    double ra = 1.* maxn * SERVO_MIN_PERIOD / T;
    double rb = 1.* maxn * SERVO_MAX_PERIOD / T;
    int duty = 2.*(rb-ra) / (SERVO_MAX_ANGLE - SERVO_MIN_ANLGLE) * (angle - SERVO_MIN_ANLGLE) + ra/2.;
    return duty;
}

static const char *TAG = "WEBSOCKET";

int angles[NDOF] = {0};
const int pwmChannels[NDOF] = {0, 1, 2, 3, 4, 5};
const int pwmPins[NDOF] = {13, 12, 14, 25, 26, 27}; ///robot pwmPins bottom-> top
ledc_channel_config_t ledc_channel[NDOF];
ledc_timer_config_t ledc_timer = {
        .duty_resolution = RESOLUTION, // resolution of PWM duty
        .freq_hz = SERVO_HZ,                      // frequency of PWM signal
        .speed_mode = LEDC_LOW_SPEED_MODE,           // timer mode
        .timer_num = SERVO_TIMER_INDEX,            // timer index
        .clk_cfg = LEDC_AUTO_CLK,              // Auto select the source clock
    };;

esp_websocket_client_handle_t wsClient;

void sock_log_handeling(int32_t event_id, esp_websocket_event_data_t *data, int level);


void pwmSetup()
{
    // setting PWM properties
    ledc_timer_config(&ledc_timer);

    for(int i = 0; i < NDOF; i++){
        ledc_channel[i].channel    = pwmChannels[i];
        ledc_channel[i].duty       = 0;
        ledc_channel[i].gpio_num   = pwmPins[i];
        ledc_channel[i].speed_mode = LEDC_LOW_SPEED_MODE;
        ledc_channel[i].hpoint     = 0;
        ledc_channel[i].timer_sel  = SERVO_TIMER_INDEX;
        ledc_channel_config(&ledc_channel[i]);
    }

}

void updateAngles()
{
    for(int i = 0; i < NDOF; i++)
    {
        int duty = angle2duty(angles[i]);
        ledc_set_duty(ledc_channel[i].speed_mode, ledc_channel[i].channel, duty);
        ledc_update_duty(ledc_channel[i].speed_mode, ledc_channel[i].channel);
    }
}


void initWifi()
{
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    esp_wifi_init(&cfg);
    esp_wifi_set_storage(WIFI_STORAGE_RAM);
    wifi_config_t wifi_config = {
        .sta = {
            .ssid = myWSSID,
            .password = myWPASS,
        },
    };
    printf("Connecting to %s...", wifi_config.sta.ssid);
    esp_wifi_set_mode(WIFI_MODE_STA);
    esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config);
    esp_wifi_start();
    esp_wifi_connect();
    printf("done wifi setup");
}

static void websocket_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    // esp_websocket_client_handle_t client = (esp_websocket_client_handle_t)handler_args;
    esp_websocket_event_data_t *data = (esp_websocket_event_data_t *)event_data;
    sock_log_handeling(event_id, data, 3);
    if(event_id != WEBSOCKET_EVENT_DATA || data->op_code != 1) return;
    char *s = (char*)data->data_ptr;
    int n = data->data_len;
    s[n] = '\0';
    printf("got message: %s\n", s);
    if(s[0] != '_') return;

    if(strncmp(s, "_angle", 6) == 0)
    {
        int wrist, angle;
        sscanf(s+7, "%d%d", &wrist, &angle);
        printf("moving %d at %d\n", wrist, angle);
        angles[wrist] = angle;
        updateAngles();
    }
}

static esp_websocket_client_handle_t websocket_app_start(void)
{
    ESP_LOGI(TAG, "Connectiong to %s...", WEBSOCKET_ECHO_ENDPOINT);
    const esp_websocket_client_config_t websocket_cfg = {
        .uri = WEBSOCKET_ECHO_ENDPOINT, 
    };
    esp_websocket_client_handle_t client = esp_websocket_client_init(&websocket_cfg);
    esp_websocket_register_events(client, WEBSOCKET_EVENT_ANY, websocket_event_handler, (void *)client);
    esp_websocket_client_start(client);
    return client;
}

void app_main(void)
{
    printf("Hello Farm!\n");
    ESP_ERROR_CHECK( nvs_flash_init() );
    tcpip_adapter_init();
    esp_event_loop_create_default();
    initWifi();
    pwmSetup();

    wsClient = websocket_app_start();

    while(1)
    {
        updateAngles();
        vTaskDelay(50 / portTICK_RATE_MS);
    }
   
}


void sock_log_handeling(int32_t event_id, esp_websocket_event_data_t *data, int level)
{
    if(level <= 0) return;
    switch (event_id) {
        case WEBSOCKET_EVENT_CONNECTED:
            ESP_LOGI(TAG, "WEBSOCKET_EVENT_CONNECTED");
            break;
        case WEBSOCKET_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "WEBSOCKET_EVENT_DISCONNECTED");
            break;
        case WEBSOCKET_EVENT_DATA:
            if(data->op_code == 10 && level < 3)
                break;
            if(level >= 2)
            {
                ESP_LOGI(TAG, "WEBSOCKET_EVENT_DATA");
                ESP_LOGI(TAG, "Received opcode=%d", data->op_code);
                ESP_LOGI(TAG, "Received=%.*s\r\n", data->data_len, (char*)data->data_ptr);
            }
            break;
        case WEBSOCKET_EVENT_ERROR:
            ESP_LOGI(TAG, "WEBSOCKET_EVENT_ERROR");
            break;
    }
}

#include <arduino.h>
#include <Servo.h>
 
int nservos = 6;
int servoPins[10] = {4, 5, 6, 7, 8, 9};
Servo servos[10];  
 
int servoAngle = 0;   // servo position in degrees
 
void setup()
{
  Serial.begin(9600);  
  for(int i = 0; i < nservos; ++i)
      servos[i].attach(servoPins[i]);
}
 
 
void loop()
{
//control the servo's direction and the position of the motor
   for(int i = 0; i < nservos; ++i)
   {
    servos[i].write(45);      // Turn SG90 servo Left to 45 degrees
    delay(1000);          // Wait 1 second
    servos[i].write(90);      // Turn SG90 servos[i] back to 90 degrees (center position)
    delay(1000);          // Wait 1 second
    servos[i].write(135);     // Turn SG90 servos[i] Right to 135 degrees
    delay(1000);          // Wait 1 second
    servos[i].write(90);      // Turn SG90 servo back to 90 degrees (center position)
    delay(1000);

   }
 
// //end control the servo's direction and the position of the motor
 
 
// //control the servo's speed  
 
// //if you change the delay value (from example change 50 to 10), the speed of the servo changes
//   for(servoAngle = 0; servoAngle < 180; servoAngle++)  //move the micro servo from 0 degrees to 180 degrees
//   {                                  
//     servo.write(servoAngle);              
//     delay(50);                  
//   }
 
//   for(servoAngle = 180; servoAngle > 0; servoAngle--)  //now move back the micro servo from 0 degrees to 180 degrees
//   {                                
//     servo.write(servoAngle);          
//     delay(10);      
//   }
//   //end control the servo's speed  
}
